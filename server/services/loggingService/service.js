const express = require("express");
const bodyParser = require("body-parser");
const redis = require("redis");
const fs = require("fs");

const app = express();

const PORT = 3001;
const HOST = "localhost";

const client = redis.createClient();
client.connect();
const subscribe = client.duplicate();
subscribe.connect();

app.use(bodyParser.json());

subscribe.subscribe("user", (message) => {
  fs.writeFileSync("user_logs.txt", message + "\n", {
    flag: "a",
    encoding: "utf8",
  });
app.post("/logs", async (req, res) => {
  if (!subscribe) {
    res.status(500).send("Error while logging data");
  }

  console.log("User data logged successfully");
  res.status(200).json("Logs added successfully");
});
	
})
   

app.listen(PORT, async () => {
  console.log(`Server start: ${HOST}:${PORT}`);
});
