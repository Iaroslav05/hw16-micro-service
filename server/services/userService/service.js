const express = require("express");
const bodyParser = require("body-parser");
const redis = require("redis");

const app = express();

const PORT = 3000;
const HOST = "localhost";

const client = redis.createClient();

app.use(bodyParser.json());

app.post("/user", async (req, res) => {
  const { id, email, password } = req.body;

  let user = await client.set(`users: ${id}`, JSON.stringify({ email, password }));

  if (!user) {

    res.status(500).send("Internal Server Error");
  }
  client.publish("user", JSON.stringify({ id, email, password }));
    res.status(201).send("User added successfully");
  });

app.get("/user/:id", async(req, res) => {
  const { id } = req.params;

  let singlUser = await client.get(`users: ${id}`);

  if (!singlUser) {
    res.status(500).send("Internal Server Error");
    return;
  }
  if (!singlUser) {
    res.status(404).send("User not found");
    return;
  }
  res.status(200).json(singlUser);
});

//   const subscribe = client.duplicate();
//   subscribe.connect();

//   subscribe.subscribe("user",(message) => {
// 	  console.log(message);
//   });


app.listen(PORT, async () => {
  await client.connect();
  
  client.on("error", (err) => {
    console.error("Redis Client not connected", err);
  });
  console.log(`Server start: ${HOST}:${PORT}`);
});
